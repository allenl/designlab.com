---
name: Get started
---

Thank you for your interest in contributing to the Pajamas Design System! We welcome all feedback regarding designs, guidelines, and implementation. The following details how to contribute in an easy and efficient way.

## Contribute an issue

If you are interested in contributing to our design system, the first step is ensuring that an issue exists in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues). There are many applications that an issue can have, including:

*   Submitting feature proposals
*   Asking questions
*   Reporting bugs and malfunctions
*   Obtaining support
*   Enhancing code implementations

If you were unable to find a related issue in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues), begin by [creating a new one](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/new). Creating an issue for each change allows us to easily track all proposals in one place. For changes that affect visual designs or user experiences, it can be helpful to include a mockup.

### UX proposals

If your proposal involves a new guideline or UX paradigm, ping a [UX reviewer or maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com) to review and provide feedback.

### Frontend proposals

If your proposal involves a change to the frontend implementation, ping a [frontend reviewer or maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com) to review and provide feedback.

## Contribute a merge request

To make changes within our Design System, follow these instructions:

1.  Choose an issue to work on. If one does not exist, please review the contribution guidelines regarding creating a new one. This opens the conversation and allows feedback to happen early, preventing risks such as duplicated or unnecessary work. It can be helpful to comment in the issue to verify that no one is working on it and that the issue is still relevant.
2.  Fork [this project](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com).
3.  Make changes reflecting the issue you’ve chosen to work on.
4.  Create a merge request. The earlier you open a merge request, the sooner you can get feedback. You can mark it as a Work in Progress to signal that you’re not done yet.
5.  Get your merge request approved. If your changes involved a new guideline or UX paradigm, then ping a [UX reviewer or maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com) to approve your changes. If your changes involve an update to frontend implementation, ping a [frontend reviewer or maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com) to approve. Some merge requests will require both a UX and frontend approver.
6.  Get your changes merged! After the necessary approvals have been added, a UX or frontend maintainer can then merge your merge request. 🙌
